<?php
/**
 * Plugin Name: WP Team Showcase and Slider
 * Plugin URI: http://www.wponlinesupport.com/
 * Text Domain: wp-team-showcase-and-slider
 * Domain Path: /languages/
 * Description: Easy to add and display your employees, team members in Grid view, Slider view and in widget. 
 * Author: WP Online Support
 * Version: 1.5
 * Author URI: http://www.wponlinesupport.com/
 *
 * @package WordPress
 * @author WP Online Support
 */
if ( ! defined( 'ABSPATH' ) ) exit;

if( !defined( 'WP_TSAS_VERSION' ) ) {
	define( 'WP_TSAS_VERSION', '1.5' ); // Version of plugin
}
if( !defined( 'WP_TSAS_DIR' ) ) {
    define( 'WP_TSAS_DIR', dirname( __FILE__ ) ); // Plugin dir
}
if(!defined( 'WP_TSAS_POST_TYPE' ) ) {
	define('WP_TSAS_POST_TYPE', 'team_showcase_post'); // Plugin post type
}
if( !defined( 'WP_TSAS_URL' ) ) {
    define( 'WP_TSAS_URL', plugin_dir_url( __FILE__ ) ); // Plugin url
}

add_action('plugins_loaded', 'wp_tsas_load_textdomain');
function wp_tsas_load_textdomain() {
	load_plugin_textdomain( 'wp-team-showcase-and-slider', false, dirname( plugin_basename(__FILE__) ) . '/languages/' );
}

/**
 * Function to get unique number value
 * 
 * @package WP Team Showcase and Slider
 * @since 1.0.1
 */
function wp_tsas_get_unique() {
  	static $unique = 0;
	$unique++;

  	return $unique;
}
require_once( 'includes/class-tsas-script.php' );
require_once( 'includes/tsas-functions.php' );
require_once( 'includes/tsas-post-type.php' );
require_once( 'includes/shortcodes/tsas-shortcode.php' );
require_once( 'includes/shortcodes/tsas-slider-shortcode.php' );

// Admin file Free Vs Pro
require_once( 'includes/admin/class-tsas-admin.php' );

// How it work file, Load admin files
if ( is_admin() || ( defined( 'WP_CLI' ) && WP_CLI ) ) {
    require_once( WP_TSAS_DIR . '/includes/admin/wp_tsas-how-it-work.php' );
}